package warrior;

public class WarriorGame{

	public static void main(String[] args)  throws InterruptedException{
		Warrior thor = new Warrior("Thor", 800, 130, 40);
		Warrior loci = new DodgeWarrior("Loci", 800, 85, 40, .35);
		Battle.startFight(thor, loci);

	}

}
